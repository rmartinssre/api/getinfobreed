# handler.py
import logging
import json
def hello(event, context):
    print(json.dumps(event));
    response = {
        "statusCode": 200,
        "body": 'Hello, world!'
    }
    return response

def goodbye(event, context):
    response = {
        "statusCode": 200,
        "body": 'Thanks'
    }

    return response

logging.basicConfig(format='%(asctime)s %(message)s')
logging.warning('is when this event was logged.')
    